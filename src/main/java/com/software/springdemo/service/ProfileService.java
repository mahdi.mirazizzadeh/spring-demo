package com.software.springdemo.service;

import com.software.springdemo.model.Profile;
import com.software.springdemo.repository.ProfileRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProfileService implements GeneralService<Profile> {

    private final ProfileRepository profileRepository;

    @Override
    public List<Profile> select() {
        return profileRepository.findAll();
    }

    @Override
    public Profile selectByID(Long id) {
        return profileRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Profile does not exists."));
    }

    @Override
    public Profile selectByEmail(String email) {
        return profileRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("Profile does not exists."));
    }

    @Override
    public Profile save(Profile profile) {
        return profileRepository.save(profile);
    }

    @Override
    public Profile update(Profile profile) {

        Profile databaseProfile = profileRepository.
                findById(profile.getId()).orElseThrow(
                        () -> new EntityNotFoundException("Entity not found for update"));

        databaseProfile.setFirstname(profile.getFirstname());
        databaseProfile.setLastname(profile.getLastname());
        databaseProfile.setAge(profile.getAge());
        databaseProfile.setEmail(profile.getEmail());
        databaseProfile.setMarried(profile.isMarried());


        return profileRepository.save(databaseProfile);
    }

    @Override
    public void delete(Long id) {
        profileRepository.deleteById(id);
    }
}
