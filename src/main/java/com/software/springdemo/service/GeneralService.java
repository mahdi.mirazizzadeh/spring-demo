package com.software.springdemo.service;

import java.util.List;

public interface GeneralService<T> {

    public List<T> select();

    public T selectByID(Long id);

    public T selectByEmail(String email);

    T save(T t);

    T update(T t);

    void delete(Long id);

}
