package com.software.springdemo.controller;

import com.software.springdemo.model.Profile;
import com.software.springdemo.service.GeneralService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/profiles")
@RequiredArgsConstructor
public class ProfileController {

    private final GeneralService<Profile> generalService;

    @GetMapping
    public ResponseEntity<?> select() {
        return ResponseEntity
                .ok().body(generalService.select());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> selectById(@PathVariable("id") Long id) {
        return ResponseEntity
                .ok().body(generalService.selectByID(id));
    }

    @GetMapping("/select/{email}")
    public ResponseEntity<?> selectByEmail(@PathVariable("email") String email) {
        return ResponseEntity
                .ok().body(generalService.selectByEmail(email));
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Profile profile) {
        return ResponseEntity
                .ok().body(generalService.save(profile));
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Profile profile) {
        return ResponseEntity
                .ok().body(generalService.update(profile));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {

        generalService.delete(id);

        return ResponseEntity.ok("Message Deleted");
    }


}
